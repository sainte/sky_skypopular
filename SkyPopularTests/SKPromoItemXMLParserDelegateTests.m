//
//  SKPromoItemXMLParserDelegateTests.m
//  SkyPopular
//
//  Created by Tiago Bencardino on 04/09/16.
//  Copyright © 2016 Sky. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "SKPromoItemXMLParserDelegate.h"
#import "SKPromoItem.h"

@interface SKPromoItemXMLParserDelegateTests : XCTestCase

@property (nonatomic, strong) SKPromoItemXMLParserDelegate *parserDelegate;

@end

@implementation SKPromoItemXMLParserDelegateTests

static NSData *XMLData;

+ (void)setUp {
    
    [super setUp];
    [self loadXMLOnXMLParser];
}

/** This static method loads our xml data once. No need to repeat this step for each test. */
+ (void)loadXMLOnXMLParser {
    
    NSBundle *testBundle = [NSBundle bundleForClass:[self class]];
    NSURL *promoItemsXMLURL = [testBundle URLForResource:@"testPromoItems" withExtension:@"xml"];
    XMLData = [NSData dataWithContentsOfURL:promoItemsXMLURL];
}

- (void)setUp {
    
    [super setUp];
    NSXMLParser *xmlParser = [[NSXMLParser alloc] initWithData:XMLData];
    self.parserDelegate = [[SKPromoItemXMLParserDelegate alloc] initWithXMLParser:xmlParser];
}

- (void)tearDown {
    
    [super tearDown];
}


- (void)testParseValidXML {
    
    [self.parserDelegate startParserWithSuccess:^(id promoItems) {
        XCTAssertEqual([promoItems count], 78, @"Parser returned wrong number of promo items");
    } failure:^(NSError *error) {
        XCTFail(@"Failure on parse XML");
    }];
}

- (void)testCorrectMapForFirstPromoItem {
    
    SKPromoItem *expectedPromoItem = [[SKPromoItem alloc] initWithTitle:@"The Bad Education Movie"
                                                       shortDescription:@"Jack Whitehall's inept teacher takes his class on a chaotic school trip to Cornwall. Based on the hit TV series." broadcastChannel:@"Sky Cinema"
                                                       detailsURLString:@"/vod/content/GOIOMOVIES/content/videoId/cc642aa0b35b4510VgnVCM1000000b43150a________/content/videoDetailsPage.do.xml"];
    
    
    [self.parserDelegate startParserWithSuccess:^(id promoItems) {
        SKPromoItem *promoItem = [promoItems firstObject];
        XCTAssertEqualObjects(promoItem.title, expectedPromoItem.title, @"Parsed promo item has wrong title.");
        XCTAssertEqualObjects(promoItem.shortDescription, expectedPromoItem.shortDescription, @"Parsed promo item has wrong short description.");
        XCTAssertEqualObjects(promoItem.broadcastChannel, expectedPromoItem.broadcastChannel, @"Parsed promo item has wrong broadcast channel.");
        XCTAssertEqualObjects(promoItem.detailsURLString, expectedPromoItem.detailsURLString, @"Parsed promo item has wrong details link.");
    } failure:^(NSError *error) {
        XCTFail(@"Failure on parse XML");
    }];
}

- (void)testParseXMLPerformance {
    
    [self measureBlock:^{
        
        [self.parserDelegate startParserWithSuccess:^(id promoItems) {
            XCTAssertGreaterThan([promoItems count], 0, "Parser XML failed - no promo items");
            
        } failure:^(NSError *error) {
            XCTFail(@"fail");
        }];
    }];
}

@end
