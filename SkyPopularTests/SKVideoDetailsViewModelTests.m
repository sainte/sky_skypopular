//
//  SKVideoDetailsViewModelTests.m
//  SkyPopular
//
//  Created by Tiago Bencardino on 04/09/16.
//  Copyright © 2016 Sky. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "SKVideoDetails.h"
#import "SKVideoDetailsViewModel.h"

@interface SKVideoDetailsViewModelTests : XCTestCase

@property (nonatomic, strong) SKVideoDetails *videoDetails;
@property (nonatomic, strong) SKVideoDetailsViewModel *viewModel;

@property (nonatomic, strong) XCTestExpectation *fetchExpectation;

@end

@implementation SKVideoDetailsViewModelTests

- (void)setUp {
    
    [super setUp];
    
    self.videoDetails = [[SKVideoDetails alloc] initWithTitle:@"The Bad Education Movie"
                                                     synopsis:@"Jack Whitehall sets a typically terrible example as posh history teacher Alfie Wickers in this feature-length outing for the hit TV comedy. Following an eye-opening visit to Amsterdam, Mr Wickers takes his class of unruly scruffy 'uns on an end-of-term trip to Cornwall. Needless to say, lessons are rudely learnt on both trips, involving dodgy locals, swans with attitude, and hamsters disappearing where the sun don't shine."
                                                        image:@"http://www.asset1.net/tv/pictures/450/250/movie/the-bad-education-movie-2015/badeducationthemovie-DI-1.jpg"
                                             broadcastChannel:@"Sky Cinema"];
    
    self.viewModel = [[SKVideoDetailsViewModel alloc] initWithURL:@"/vod/content/GOIOMOVIES/content/videoId/cc642aa0b35b4510VgnVCM1000000b43150a________/content/videoDetailsPage.do.xml" videoDetails:self.videoDetails];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

#pragma mark - test bindings

- (void)testTitleBindingIsCorrect {
    
    NSString *expectedValue = self.videoDetails.title;
    NSString *value = [self.viewModel title];
    XCTAssertEqual(value, expectedValue, @"Title is not the one expected");
}

- (void)testSynopsisIsCorrect {
    
    NSString *expectedValue = self.videoDetails.synopsis;
    NSString *value = [self.viewModel synopsis];
    XCTAssertEqual(value, expectedValue, @"Synopsis is not the one expected");
}

- (void)testBroadcastChannelBindingIsCorrect {
    
    NSString *expectedValue = self.videoDetails.broadcastChannel;
    NSString *value = [self.viewModel broadcastChannel];
    XCTAssertEqual(value, expectedValue, @"Broadcast channel is not the one expected");
}

- (void)testImagelBindingIsCorrect {
    
    NSString *expectedValue = self.videoDetails.image;
    NSString *value = [self.viewModel imageURL];
    XCTAssertEqual(value, expectedValue, @"Image URL is not the one expected");
}

#pragma mark - test fetch

- (void)refresh:(NSNotification *)notification {
    
    SKVideoDetails *videoDetails = notification.object;
    XCTAssertNotNil(videoDetails, @"Video details fetched is nil.");
    
    [self.fetchExpectation fulfill];
    [self removeObservers];
}

- (void)refreshFailed:(NSNotification *)notification {
    
    NSString *errorMessage = notification.object;
    XCTAssertNotNil(errorMessage, @"Error is nil");
    
    [self.fetchExpectation fulfill];
    [self removeObservers];
}

- (void)testFetchVideoDetails {
    
    [self addObservers];
    
    self.fetchExpectation = [self expectationWithDescription:@"fetch"];
    
    SKVideoDetailsViewModel *videoDetailsViewModel = [[SKVideoDetailsViewModel alloc] initWithURL:@"/vod/content/GOIOMOVIES/content/videoId/cc642aa0b35b4510VgnVCM1000000b43150a________/content/videoDetailsPage.do.xml"];
    [videoDetailsViewModel fetchDetails];
    
    [self waitForExpectationsWithTimeout:10 handler:^(NSError * _Nullable error) {
        NSLog(@"error: %@", error.description);
    }];
}

- (void)addObservers {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refresh:) name:videoDetailsDidChanged object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshFailed:) name:videoDetailsDidFailed object:nil];
}

- (void)removeObservers {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refresh:) name:videoDetailsDidChanged object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshFailed:) name:videoDetailsDidFailed object:nil];
}

@end
