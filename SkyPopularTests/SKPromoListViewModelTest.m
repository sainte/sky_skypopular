//
//  SKPromoListViewModelTest.m
//  SkyPopular
//
//  Created by Tiago Bencardino on 04/09/16.
//  Copyright © 2016 Sky. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "SKPromoListViewModel.h"
#import "SKPromoItem.h"

@interface SKPromoListViewModelTest : XCTestCase

@property (nonatomic, strong) NSArray *promoItems;
@property (nonatomic, strong) SKPromoListViewModel *viewModel;

@property (nonatomic, strong) XCTestExpectation *fetchExpectation;

@end

@implementation SKPromoListViewModelTest

- (void)setUp {
    
    [super setUp];
    
    [self makeSomePromoItems];
    self.viewModel = [[SKPromoListViewModel alloc] initWithPromoItems:self.promoItems];
}

- (void)makeSomePromoItems {
    
    SKPromoItem *promoItemOne = [[SKPromoItem alloc] initWithTitle:@"The Bad Education Movie"
                                                  shortDescription:@"Jack Whitehall's inept teacher takes his class on a chaotic school trip to Cornwall. Based on the hit TV series." broadcastChannel:@"Sky Cinema"
                                                  detailsURLString:@"/vod/content/GOIOMOVIES/content/videoId/cc642aa0b35b4510VgnVCM1000000b43150a________/content/videoDetailsPage.do.xml"];
    SKPromoItem *promoItemTwo = [[SKPromoItem alloc] initWithTitle:@"The Dressmaker"
                                                  shortDescription:@"Kate Winslet's glamorous fashionista returns to her sleepy hometown to settle old scores. Offbeat revenge drama with Liam Hemsworth and Hugo Weaving."
                                                  broadcastChannel:@"Sky Cinema"
                                                  detailsURLString:@"/vod/content/GOIOMOVIES/content/videoId/3b771dc39fe76510VgnVCM1000000b43150a________/content/videoDetailsPage.do.xml"];
    SKPromoItem *promoItemThree = [[SKPromoItem alloc] initWithTitle:@"The Jungle Book 2"
                                                    shortDescription:@"Mowgli and Baloo go in search of more bare necessities while giving tiger Shere Khan the slip in this follow-up to the classic Disney adventure."
                                                    broadcastChannel:@"Disney Movies"
                                                    detailsURLString:@"/vod/content/GOIOMOVIES/content/videoId/817ffd12ab391410VgnVCM1000000b43150a________/content/videoDetailsPage.do.xml"];
    
    self.promoItems = @[promoItemOne, promoItemTwo, promoItemThree];
}

- (void)tearDown {
    
    [super tearDown];
}

- (void)testCountOfPromoItems {
    
    NSInteger count = [self.viewModel countOfPromoItems];
    XCTAssertEqual(count, 3, @"Wrong number of Promo items");
}

- (void)testVideoDetailsViewModelIsGenerated {
    
    id videoDetails = [self.viewModel videoDetailsViewModerForIndex:2];
    XCTAssertNotNil(videoDetails, @"View model for correspondent video details is nil.");
}

#pragma mark - test bindings

- (void)testTitleBindingIsCorrect {
    
    NSString *expectedValue = [self.promoItems[2] title];
    NSString *value = [self.viewModel titleForPromoItemAtIndex:2];
    XCTAssertEqual(value, expectedValue, @"Title is not the one expected");
}

- (void)testShortDescriptionIsCorrect {
    
    NSString *expectedValue = [self.promoItems[2] shortDescription];
    NSString *value = [self.viewModel shortDescriptionForPromoItemAtIndex:2];
    XCTAssertEqual(value, expectedValue, @"Short description is not the one expected");
}

- (void)testBroadcastChannelBindingIsCorrect {
    
    NSString *expectedValue = [self.promoItems[2] broadcastChannel];
    NSString *value = [self.viewModel broadcastChannelForPromoItemAtIndex:2];
    XCTAssertEqual(value, expectedValue, @"Broadcast channel is not the one expected");
}

#pragma mark - test fetch

- (void)refresh:(NSNotification *)notification {
    
    NSArray *promoItems = notification.object;
    XCTAssertGreaterThan([promoItems count], 0, @"No items returned from server");
    
    [self.fetchExpectation fulfill];
    [self removeObservers];
}

- (void)refreshFailed:(NSNotification *)notification {
    
    NSString *errorMessage = notification.object;
    XCTAssertNotNil(errorMessage, @"Error is nil");
    
    [self.fetchExpectation fulfill];
    [self removeObservers];
}

- (void)testFetchPromoItems {
    
    [self addObservers];
    
    self.fetchExpectation = [self expectationWithDescription:@"fetch"];
    
    SKPromoListViewModel *promoListViewModel = [SKPromoListViewModel new];
    [promoListViewModel fetchPromoItems];
    
    [self waitForExpectationsWithTimeout:10 handler:^(NSError * _Nullable error) {
        NSLog(@"error: %@", error.description);
    }];
}

- (void)addObservers {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refresh:) name:promoItemDidChanged object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshFailed:) name:promoItemDidFailed object:nil];
}

- (void)removeObservers {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:promoItemDidChanged object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:promoItemDidFailed object:nil];
}

@end
