//
//  SKVideoDetailsXMLParserDelegateTests.m
//  SkyPopular
//
//  Created by Tiago Bencardino on 04/09/16.
//  Copyright © 2016 Sky. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "SKVideoDetailsXMLParserDelegate.h"
#import "SKVideoDetails.h"

@interface SKVideoDetailsXMLParserDelegateTests : XCTestCase

@property (nonatomic, strong) SKVideoDetailsXMLParserDelegate *parserDelegate;

@end

@implementation SKVideoDetailsXMLParserDelegateTests

static NSData *XMLData;

+ (void)setUp {
    
    [super setUp];
    [self loadXMLOnXMLParser];
}

/** This static method loads our xml data once. No need to repeat this step for each test. */
+ (void)loadXMLOnXMLParser {
    
    NSBundle *testBundle = [NSBundle bundleForClass:[self class]];
    NSURL *promoItemsXMLURL = [testBundle URLForResource:@"testVideoDetails" withExtension:@"xml"];
    XMLData = [NSData dataWithContentsOfURL:promoItemsXMLURL];
}

- (void)setUp {
    
    [super setUp];
    NSXMLParser *xmlParser = [[NSXMLParser alloc] initWithData:XMLData];
    self.parserDelegate = [[SKVideoDetailsXMLParserDelegate alloc] initWithXMLParser:xmlParser];
}

- (void)tearDown {
    
    [super tearDown];
}

- (void)testParseValidXML {
    
    [self.parserDelegate startParserWithSuccess:^(id videoDetails) {
        XCTAssertNotNil(videoDetails, @"Video details is nil");
    } failure:^(NSError *error) {
        XCTFail(@"Failure on parse XML");
    }];
}

- (void)testCorrectMapForVideoDetail {
    
    SKVideoDetails *expectedVideoDetails = [[SKVideoDetails alloc] initWithTitle:@"The Bad Education Movie"
                                                                        synopsis:@"Jack Whitehall sets a typically terrible example as posh history teacher Alfie Wickers in this feature-length outing for the hit TV comedy. Following an eye-opening visit to Amsterdam, Mr Wickers takes his class of unruly scruffy 'uns on an end-of-term trip to Cornwall. Needless to say, lessons are rudely learnt on both trips, involving dodgy locals, swans with attitude, and hamsters disappearing where the sun don't shine."
                                                                           image:@"http://www.asset1.net/tv/pictures/450/250/movie/the-bad-education-movie-2015/badeducationthemovie-DI-1.jpg"
                                                                broadcastChannel:@"Sky Cinema"];
    
    [self.parserDelegate startParserWithSuccess:^(id object) {
        SKVideoDetails *videoDetails = (SKVideoDetails*)object;
        XCTAssertEqualObjects(videoDetails.title, expectedVideoDetails.title, @"Parsed video details has wrong title.");
        XCTAssertEqualObjects(videoDetails.synopsis, expectedVideoDetails.synopsis, @"Parsed video details has wrong synopsis.");
        XCTAssertEqualObjects(videoDetails.broadcastChannel, expectedVideoDetails.broadcastChannel, @"Parsed video details has wrong broadcast channel.");
        XCTAssertEqualObjects(videoDetails.image, expectedVideoDetails.image, @"Parsed video details has wrong image");
    } failure:^(NSError *error) {
        XCTFail(@"Failure on parse XML");
    }];
}

- (void)testParseXMLPerformance {
    
    [self measureBlock:^{
        
        [self.parserDelegate startParserWithSuccess:^(id object) {
            XCTAssertNotNil(object, "Parser XML failed - no object");
            
        } failure:^(NSError *error) {
            XCTFail(@"fail");
        }];
    }];
}

@end
