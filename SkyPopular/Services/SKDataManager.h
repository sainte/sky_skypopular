//
//  SKDataManager.h
//  Favorites
//
//  Created by Tiago Bencardino on 03/09/16.
//  Copyright © 2016 Sky. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^successResponse)(id object);
typedef void(^failureResponse)(NSError *error);

@interface SKDataManager : NSObject

- (void)getPromoItemsWithSuccess:(successResponse)success failure:(failureResponse)failure;
- (void)getVideoDetailsWithUrl:(NSString*)videoDetailsURL success:(successResponse)success failure:(failureResponse)failure;

@end
