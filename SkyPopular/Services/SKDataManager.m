//
//  SKDataManager.m
//  Favorites
//
//  Created by Tiago Bencardino on 03/09/16.
//  Copyright © 2016 Sky. All rights reserved.
//

#import "SKDataManager.h"
#import <AFNetworking/AFNetworking.h>
#import "SKPromoItemXMLParserDelegate.h"
#import "SKVideoDetailsXMLParserDelegate.h"

NSString * const goioSKYHost = @"http://goio.sky.com/";
NSString * const promoItemsLink = @"/vod/content/Home/Application_Navigation/Sky_Movies/Most_Popular/content/promoPage.do.xml";

@implementation SKDataManager

- (void)getPromoItemsWithSuccess:(successResponse)success failure:(failureResponse)failure {
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer  = [AFXMLParserResponseSerializer serializer];
    
    NSString *path = [goioSKYHost stringByAppendingString:promoItemsLink];
    
    [manager GET:path parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        if ([responseObject isKindOfClass:[NSXMLParser class]]) {
            
            SKPromoItemXMLParserDelegate *parserDelegate = [[SKPromoItemXMLParserDelegate alloc] initWithXMLParser:responseObject];
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                [parserDelegate startParserWithSuccess:success failure:failure];
            });

        } else {
            failure(nil);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(error);
    }];
}

- (void)getVideoDetailsWithUrl:(NSString*)videoDetailsURL success:(successResponse)success failure:(failureResponse)failure {
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer  = [AFXMLParserResponseSerializer serializer];
    
    NSString *path = [goioSKYHost stringByAppendingString:videoDetailsURL];
    [manager GET:path parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        if ([responseObject isKindOfClass:[NSXMLParser class]]) {
            
            SKVideoDetailsXMLParserDelegate *parserDelegate = [[SKVideoDetailsXMLParserDelegate alloc] initWithXMLParser:responseObject];
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                [parserDelegate startParserWithSuccess:success failure:failure];
            });

        } else {
            failure(nil);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(error);
    }];
}
@end
