//
//  NSError+SKErrorMessage.m
//  SkyPopular
//
//  Created by Tiago Bencardino on 04/09/16.
//  Copyright © 2016 Sky. All rights reserved.
//

#import "NSError+SKErrorMessage.h"

@implementation NSError (SKErrorMessage)

- (NSString*)sk_errorMessage {
    
    NSInteger statusCode = self.code;
    NSString *errorMessage;
    
    if(statusCode == -1001) {
        errorMessage = @"Connection timeout. Please check your connection and try it again.";
    } else if (statusCode == -1009 || statusCode == -1004) {
        errorMessage = @"The Internet connection appears to be offline. Please check your connection and try it again.";
    } else {
        errorMessage = @"Oops, something went wrong and it's not your fault.";
    }
    return errorMessage;
}

@end
