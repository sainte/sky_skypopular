//
//  NSError+SKErrorMessage.h
//  SkyPopular
//
//  Created by Tiago Bencardino on 04/09/16.
//  Copyright © 2016 Sky. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSError (SKErrorMessage)

- (NSString*)sk_errorMessage;

@end
