//
//  SKPromoItem.h
//  Favorites
//
//  Created by Tiago Bencardino on 03/09/16.
//  Copyright © 2016 Sky. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SKPromoItem : NSObject

@property (nonatomic, strong) NSString *broadcastChannel;
@property (nonatomic, strong) NSString *shortDescription;
@property (nonatomic, strong) NSString *title;

@property (nonatomic, strong) NSString *detailsURLString;

- (id)initWithTitle:(NSString*)title shortDescription:(NSString*)shortDescription broadcastChannel:(NSString*)broadcastChannel detailsURLString:(NSString*)detailsURLString;

@end
