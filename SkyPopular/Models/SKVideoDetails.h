//
//  SKVideoDetails.h
//  SkyPopular
//
//  Created by Tiago Bencardino on 03/09/16.
//  Copyright © 2016 Sky. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SKVideoDetails : NSObject

@property (nonatomic, strong) NSString *broadcastChannel;
@property (nonatomic, strong) NSString *image;
@property (nonatomic, strong) NSString *synopsis;
@property (nonatomic, strong) NSString *title;

- (id)initWithTitle:(NSString*)title synopsis:(NSString*)synopsis image:(NSString*)image broadcastChannel:(NSString*)broadcastChannel;

@end
