//
//  SKVideoDetails.m
//  SkyPopular
//
//  Created by Tiago Bencardino on 03/09/16.
//  Copyright © 2016 Sky. All rights reserved.
//

#import "SKVideoDetails.h"

@implementation SKVideoDetails

- (id)initWithTitle:(NSString *)title synopsis:(NSString *)synopsis image:(NSString *)image broadcastChannel:(NSString *)broadcastChannel {
    
    if (self = [super init]) {
        self.title = title;
        self.synopsis = synopsis;
        self.image = image;
        self.broadcastChannel = broadcastChannel;
    }
    
    return self;
}

- (NSString*)description {
    
    return [NSString stringWithFormat:@"SKVideoDetails(%@)", self.title];
}

@end
