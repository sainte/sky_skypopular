//
//  SKPromoItem.m
//  Favorites
//
//  Created by Tiago Bencardino on 03/09/16.
//  Copyright © 2016 Sky. All rights reserved.
//

#import "SKPromoItem.h"

@implementation SKPromoItem

- (id)initWithTitle:(NSString *)title shortDescription:(NSString *)shortDescription broadcastChannel:(NSString *)broadcastChannel detailsURLString:(NSString *)detailsURLString {
    
    if (self = [super init]) {
        self.title = title;
        self.shortDescription = shortDescription;
        self.broadcastChannel = broadcastChannel;
        self.detailsURLString = detailsURLString;
    }
    
    return self;
}

- (NSString*)description {
    
    return [NSString stringWithFormat:@"SKPromoItem(%@)", self.title];
}

@end
