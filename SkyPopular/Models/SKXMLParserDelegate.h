//
//  SKXMLParserDelegate.h
//  SkyPopular
//
//  Created by Tiago Bencardino on 03/09/16.
//  Copyright © 2016 Sky. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SKDataManager.h"

@interface SKXMLParserDelegate : NSOperation <NSXMLParserDelegate>

@property (nonatomic, copy) successResponse success;
@property (nonatomic, copy) failureResponse failure;

@property (nonatomic, strong) NSXMLParser *parser;

@property (nonatomic, strong) NSMutableString *currentValue;

- (id)initWithXMLParser:(NSXMLParser*)parser;
- (BOOL)startParserWithSuccess:(successResponse)success failure:(failureResponse)failure;

@end
