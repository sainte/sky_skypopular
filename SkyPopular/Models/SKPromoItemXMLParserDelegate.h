//
//  SKPromoItemXMLParserDelegate.h
//  Favorites
//
//  Created by Tiago Bencardino on 03/09/16.
//  Copyright © 2016 Sky. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SKXMLParserDelegate.h"

@interface SKPromoItemXMLParserDelegate : SKXMLParserDelegate

@end
