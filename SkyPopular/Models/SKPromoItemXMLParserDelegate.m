//
//  SKPromoItemXMLParserDelegate.m
//  Favorites
//
//  Created by Tiago Bencardino on 03/09/16.
//  Copyright © 2016 Sky. All rights reserved.
//

#import "SKPromoItemXMLParserDelegate.h"
#import "SKPromoItem.h"

@interface SKPromoItemXMLParserDelegate ()

@property (nonatomic, strong) NSMutableArray *promoItems;
@property (nonatomic, strong) SKPromoItem *currentPromoItem;
@property (nonatomic, assign) BOOL isCurrentBroadcastChannel;

@end

@implementation SKPromoItemXMLParserDelegate

#pragma mark - NSXMLParserDelegate

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary<NSString *,NSString *> *)attributeDict {
    
    if ([elementName isEqualToString:@"PromoItems"]) {
        self.promoItems = [NSMutableArray new];
        
    } else if ([elementName isEqualToString:@"item"]) {
        self.currentPromoItem = [SKPromoItem new];
        self.currentPromoItem.detailsURLString = attributeDict[@"url"];
        
    } else if ([elementName isEqualToString:@"BroadcastChannel"]) {
        self.isCurrentBroadcastChannel = YES;
    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    
    [self.currentValue appendString:string];
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    if ([elementName isEqualToString:@"item"]) {
        [self.promoItems addObject:self.currentPromoItem];
        
    } else if ([elementName isEqualToString:@"title"]) {
        self.currentPromoItem.title = [self.currentValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
    } else if ([elementName isEqualToString:@"ShortDescription"]) {
        self.currentPromoItem.shortDescription = [self.currentValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];;
        
    } else if (self.isCurrentBroadcastChannel && [elementName isEqualToString:@"Name"]) {
        self.currentPromoItem.broadcastChannel = [self.currentValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        self.isCurrentBroadcastChannel = NO;
    }
    
    self.currentValue = nil;
}

- (void)parserDidEndDocument:(NSXMLParser *)parser {
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.success(self.promoItems);
    });
}

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError {
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.failure(parseError);
    });
}

@end
