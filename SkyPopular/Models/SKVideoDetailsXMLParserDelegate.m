//
//  SKVideoDetailsXMLParserDelegate.m
//  SkyPopular
//
//  Created by Tiago Bencardino on 03/09/16.
//  Copyright © 2016 Sky. All rights reserved.
//

#import "SKVideoDetailsXMLParserDelegate.h"
#import "SKVideoDetails.h"

@interface SKVideoDetailsXMLParserDelegate ()

@property (nonatomic, strong) SKVideoDetails *videoDetails;

@property (nonatomic, assign) BOOL isCurrentBroadcastChannel;
@end

@implementation SKVideoDetailsXMLParserDelegate

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary<NSString *,NSString *> *)attributeDict {
    
    if ([elementName isEqualToString:@"sp:VideoDetails"]) {
        self.videoDetails = [SKVideoDetails new];
        
    } if ([elementName isEqualToString:@"BroadcastChannel"]) {
        self.isCurrentBroadcastChannel = YES;
    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    
    [self.currentValue appendString:string];
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    if ([elementName isEqualToString:@"Title"]) {
        self.videoDetails.title = [self.currentValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
    } else if ([elementName isEqualToString:@"Image"]) {
        self.videoDetails.image = [self.currentValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
    } else if ([elementName isEqualToString:@"Synopsis"]) {
        self.videoDetails.synopsis = [self.currentValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];;
        
    } else if (self.isCurrentBroadcastChannel && [elementName isEqualToString:@"Name"]) {
        self.videoDetails.broadcastChannel = [self.currentValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        self.isCurrentBroadcastChannel = NO;
    }
    
    self.currentValue = nil;
}

- (void)parserDidEndDocument:(NSXMLParser *)parser {
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.success(self.videoDetails);
    });
}

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError {
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.failure(parseError);
    });
}

@end
