//
//  SKXMLParserDelegate.m
//  SkyPopular
//
//  Created by Tiago Bencardino on 03/09/16.
//  Copyright © 2016 Sky. All rights reserved.
//

#import "SKXMLParserDelegate.h"

@implementation SKXMLParserDelegate

- (id)initWithXMLParser:(NSXMLParser *)parser {
    
    if (self = [super init]) {
        
        //NSXMLParser delegate is not retained so there's no retain cicle
        parser.delegate = self;
        self.parser = parser;
    }
    
    return self;
}

/** Lazy instanciation for current value. */
- (NSMutableString*)currentValue {
    
    if (!_currentValue) {
        _currentValue =  [NSMutableString new];
    }
    return _currentValue;
}

- (BOOL)startParserWithSuccess:(successResponse)success failure:(failureResponse)failure {
    
    self.success = success;
    self.failure = failure;
    
    return [self.parser parse];
}

@end
