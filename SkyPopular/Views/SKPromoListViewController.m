//
//  SKPromoListViewController.m
//  Favorites
//
//  Created by Tiago Bencardino on 03/09/16.
//  Copyright © 2016 Sky. All rights reserved.
//

#import "SKPromoListViewController.h"
#import "SKPromoListViewModel.h"
#import "SKPromoItemTableViewCell.h"
#import "SKVideoDetailsViewController.h"

NSString * const promoItemCellName = @"SKPromoItemTableViewCell";
NSString * const videoDetailsSegueIdentifier = @"SKVideoDetailsViewController";

@interface SKPromoListViewController ()

@property (nonatomic, strong) SKPromoListViewModel *viewModel;
@property (nonatomic, strong) UIRefreshControl *refreshControl;

@end

@implementation SKPromoListViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self addObservers];
    [self configureViewModel];
    [self configureTableView];
}

- (void)dealloc {
    
    [self removeObservers];
}

- (void)configureViewModel {
    
    self.viewModel = [SKPromoListViewModel new];
    [self.viewModel fetchPromoItems];
}

- (void)configureTableView {
    
    self.refreshControl = [UIRefreshControl new];
    self.refreshControl.tintColor = [UIColor whiteColor];
    [self.refreshControl addTarget:self action:@selector(fetchDetails) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.refreshControl];
    [self.refreshControl beginRefreshing];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.estimatedRowHeight = 85;
    
    [self.tableView registerNib:[UINib nibWithNibName:promoItemCellName bundle:nil] forCellReuseIdentifier:promoItemCellName];
}

- (void)fetchDetails {
    
    [self.viewModel fetchPromoItems];
}
#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [self.viewModel countOfPromoItems];
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    SKPromoItemTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:promoItemCellName];
    
    cell.titleLabel.text = [self.viewModel titleForPromoItemAtIndex:indexPath.row];
    cell.shortDescriptionLabel.text = [self.viewModel shortDescriptionForPromoItemAtIndex:indexPath.row];
    cell.broadbandChannelLabel.text = [self.viewModel broadcastChannelForPromoItemAtIndex:indexPath.row];
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [self performSegueWithIdentifier:videoDetailsSegueIdentifier sender:cell];
}

#pragma mark - refresh

- (void)refresh {
    
    [self.tableView reloadData];
    [self.refreshControl endRefreshing];
}

- (void)refreshFailed:(NSNotification*)notification {
    
    NSString *errorMessage = notification.object;
    [self.refreshControl endRefreshing];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Error" message:errorMessage preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil]];
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:videoDetailsSegueIdentifier] && [segue.destinationViewController isKindOfClass:[SKVideoDetailsViewController class]]) {
        
        NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
        ((SKVideoDetailsViewController*)segue.destinationViewController).viewModel = [self.viewModel videoDetailsViewModerForIndex:indexPath.row];
    }
}

#pragma mark - observers

- (void)addObservers {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refresh) name:promoItemDidChanged object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshFailed:) name:promoItemDidFailed object:nil];
}

- (void)removeObservers {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:promoItemDidChanged object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:promoItemDidFailed object:nil];
}
@end
