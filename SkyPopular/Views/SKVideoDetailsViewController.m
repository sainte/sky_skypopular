//
//  SKVideoDetailsViewController.m
//  SkyPopular
//
//  Created by Tiago Bencardino on 03/09/16.
//  Copyright © 2016 Sky. All rights reserved.
//

#import "SKVideoDetailsViewController.h"
#import "UIImageView+AFNetworking.h"

@interface SKVideoDetailsViewController ()

@property (strong, nonatomic) UIActivityIndicatorView *activityIndicator;

@end

@implementation SKVideoDetailsViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self addObservers];
    [self initializeDefaultValues];
    [self configureViewModel];
    [self configureNavBar];
    [self.activityIndicator startAnimating];
}

- (void)dealloc {
    
    [self removeObservers];
}

- (void)initializeDefaultValues {
    
    self.titleLabel.text = @"";
    self.synopsisLabel.text = @"";
    self.broadcastChannelLabel.text = @"";
}

- (void)configureViewModel {
    
    [self.viewModel fetchDetails];
}

- (void)configureNavBar {
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    UIBarButtonItem *refreshBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.activityIndicator];
    self.navigationItem.rightBarButtonItem = refreshBarButtonItem;
    
    [self.activityIndicator startAnimating];
}

#pragma mark - refresh

- (void)refresh {
    
    self.titleLabel.text = [self.viewModel title];
    self.synopsisLabel.text = [self.viewModel synopsis];
    self.broadcastChannelLabel.text = [self.viewModel broadcastChannel];
    
    //Async image downloader with image cache from AFNetworking
    [self.imageView setImageWithURL:[NSURL URLWithString:[self.viewModel imageURL]]];
    
    [self.activityIndicator stopAnimating];
}

- (void)refreshFailed:(NSNotification*)notification {
    
    NSString *errorMessage = notification.object;
    [self.activityIndicator stopAnimating];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Error" message:errorMessage preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil]];
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - observers

- (void)addObservers {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refresh) name:videoDetailsDidChanged object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshFailed:) name:videoDetailsDidFailed object:nil];
}

- (void)removeObservers {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refresh) name:videoDetailsDidChanged object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshFailed:) name:videoDetailsDidFailed object:nil];
}

@end
