//
//  SKVideoDetailsViewController.h
//  SkyPopular
//
//  Created by Tiago Bencardino on 03/09/16.
//  Copyright © 2016 Sky. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SKVideoDetailsViewModel.h"

@interface SKVideoDetailsViewController : UIViewController 

@property (nonatomic, strong) SKVideoDetailsViewModel *viewModel;

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *synopsisLabel;
@property (weak, nonatomic) IBOutlet UILabel *broadcastChannelLabel;

@end
