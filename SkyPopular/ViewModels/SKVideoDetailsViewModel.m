//
//  SKVideoDetailsViewModel.m
//  SkyPopular
//
//  Created by Tiago Bencardino on 03/09/16.
//  Copyright © 2016 Sky. All rights reserved.
//

#import "SKVideoDetailsViewModel.h"
#import "SKVideoDetails.h"
#import "SKDataManager.h"
#import "NSError+SKErrorMessage.h"

NSString * const videoDetailsDidChanged = @"videoDetailsDidChanged";
NSString * const videoDetailsDidFailed = @"videoDetailsDidFailed";

@interface SKVideoDetailsViewModel ()

@property (nonatomic, strong) NSString *detailsURL;
@property (nonatomic, strong) SKVideoDetails *videoDetails;

@end

@implementation SKVideoDetailsViewModel

- (id)initWithURL:(NSString *)detailsURL {
    
    if (self = [super init]) {
        self.detailsURL = detailsURL;
    }
    
    return self;
}

- (id)initWithURL:(NSString *)detailsURL videoDetails:(SKVideoDetails *)videoDetails {
    
    if (self = [super init]) {
        self.detailsURL = detailsURL;
        self.videoDetails = videoDetails;
    }
    
    return self;
}

- (void)fetchDetails {
    
    SKDataManager *dataManager = [SKDataManager new];
    [dataManager getVideoDetailsWithUrl:self.detailsURL success:^(id object) {
        self.videoDetails = object;
        [[NSNotificationCenter defaultCenter] postNotificationName:videoDetailsDidChanged object:self.videoDetails];
    } failure:^(NSError *error) {
        NSString *errorMessage = [error sk_errorMessage];
        [[NSNotificationCenter defaultCenter] postNotificationName:videoDetailsDidFailed object:errorMessage];
    }];
}



#pragma mark - binding

- (NSString*)broadcastChannel {
    
    return self.videoDetails.broadcastChannel;
}

- (NSString*)imageURL {
    
    return self.videoDetails.image;
}

- (NSString*)synopsis {
    
    return self.videoDetails.synopsis;
}

- (NSString*)title {
    
    return self.videoDetails.title;
}

@end
