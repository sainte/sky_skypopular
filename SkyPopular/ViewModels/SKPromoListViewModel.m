//
//  SKPromoListViewModel.m
//  SkyPopular
//
//  Created by Tiago Bencardino on 03/09/16.
//  Copyright © 2016 Sky. All rights reserved.
//

#import "SKPromoListViewModel.h"
#import "SKPromoItem.h"
#import "SKDataManager.h"
#import "NSError+SKErrorMessage.h"

NSString * const promoItemDidChanged = @"promoItemDidChanged";
NSString * const promoItemDidFailed = @"promoItemDidFailed";

@interface SKPromoListViewModel ()

@property (nonatomic, strong) NSArray *promoItems;

@end

@implementation SKPromoListViewModel

- (id)initWithPromoItems:(NSArray*)promoItems {
    
    if (self = [super init]) {
        self.promoItems = promoItems;
    }
    return self;
}

- (void)fetchPromoItems {
    
    SKDataManager *dataManager = [SKDataManager new];
    [dataManager getPromoItemsWithSuccess:^(id object) {
        self.promoItems = object;
        [[NSNotificationCenter defaultCenter] postNotificationName:promoItemDidChanged object:self.promoItems];
        
    } failure:^(NSError *error) {
        NSString *errorMessage = [error sk_errorMessage];
        [[NSNotificationCenter defaultCenter] postNotificationName:promoItemDidFailed object:errorMessage];
    }];
}


- (NSInteger)countOfPromoItems {
    
    return self.promoItems.count;
}

- (SKVideoDetailsViewModel*)videoDetailsViewModerForIndex:(NSInteger)index {
    
    NSString *detailsURL = [self.promoItems[index] detailsURLString];
    SKVideoDetailsViewModel *viewModel = [[SKVideoDetailsViewModel alloc] initWithURL:detailsURL];
    
    return viewModel;
}

#pragma mark - Binding

- (NSString*)titleForPromoItemAtIndex:(NSInteger)index {
    
    return [self.promoItems[index] title];
}

- (NSString*)shortDescriptionForPromoItemAtIndex:(NSInteger)index {
    
    return [self.promoItems[index] shortDescription];
}

- (NSString*)broadcastChannelForPromoItemAtIndex:(NSInteger)index {
    
    return [self.promoItems[index] broadcastChannel];
}

@end
