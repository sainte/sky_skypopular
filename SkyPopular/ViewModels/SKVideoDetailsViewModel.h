//
//  SKVideoDetailsViewModel.h
//  SkyPopular
//
//  Created by Tiago Bencardino on 03/09/16.
//  Copyright © 2016 Sky. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SKVideoDetails.h"

extern NSString * const videoDetailsDidChanged;
extern NSString * const videoDetailsDidFailed;

@interface SKVideoDetailsViewModel : NSObject

- (id)initWithURL:(NSString*)detailsURL;
- (id)initWithURL:(NSString*)detailsURL videoDetails:(SKVideoDetails*)videoDetails;

- (void)fetchDetails;

#pragma mark - binding

- (NSString*)broadcastChannel;
- (NSString*)imageURL;
- (NSString*)synopsis;
- (NSString*)title;

@end
