//
//  SKPromoListViewModel.h
//  SkyPopular
//
//  Created by Tiago Bencardino on 03/09/16.
//  Copyright © 2016 Sky. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SKVideoDetailsViewModel.h"

extern NSString * const promoItemDidChanged;
extern NSString * const promoItemDidFailed;

@interface SKPromoListViewModel : NSObject

- (id)initWithPromoItems:(NSArray*)promoItems;

- (void)fetchPromoItems;
- (NSInteger)countOfPromoItems;
- (SKVideoDetailsViewModel*)videoDetailsViewModerForIndex:(NSInteger)index;

#pragma mark - binding

- (NSString*)titleForPromoItemAtIndex:(NSInteger)index;
- (NSString*)shortDescriptionForPromoItemAtIndex:(NSInteger)index;
- (NSString*)broadcastChannelForPromoItemAtIndex:(NSInteger)index;

@end
